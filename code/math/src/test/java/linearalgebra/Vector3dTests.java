//Alec Morin    2239209
package linearalgebra;
import static org.junit.Assert.*;
import org.junit.Test;
public class Vector3dTests {
    
    @Test
    public void testGetMethods(){
        Vector3d vector = new Vector3d(3, 1, 2);
        assertEquals("Testing the get methods for x", 3, vector.getX(), 0.01);

        assertEquals("Testing the get methods for y", 1, vector.getY(), 0.01);

        assertEquals("Testing the get methods for x", 2, vector.getZ(), 0.01);
    }

    @Test
    public void testMagnitude(){
        Vector3d vector = new Vector3d(8, 4, 1);
        
        assertEquals("Testing magnitude", 9, vector.magnitude(),0.01 );
    }

    @Test
    public void testDotProduct(){
        Vector3d vector = new Vector3d(1, 1, 2);
        Vector3d vector2 = new Vector3d(2, 3, 4);
        double expected = vector.dotProduct(vector2);

        assertEquals("Testing dotProduct method", 13, expected, 0.01);
    }

    @Test
    public void testAdd(){
        Vector3d vector = new Vector3d(1, 1, 2);
        Vector3d vector2 = new Vector3d(2, 3, 4);

        Vector3d result = vector.add(vector2);

        assertEquals("Testing the X value in the add() method", 3, result.getX(), 0.01);
        assertEquals("Testing the Y value in the add() method", 4, result.getY(), 0.01);
        assertEquals("Testing the Z value in the add() method", 6, result.getZ(), 0.01);
    }
}
