/**
 * This is Alec Morin and this is the first
 * time i use this type of commenting
 * @author Alec Morin
 */
package linearalgebra;

public class Vector3d {
    
    private double x;
    private double y;
    private double z;

    /**
     * @param newX initializes the x field
     * @param newY initializes the y field
     * @param newZ initializes the z field
     */
    public Vector3d(double newX, double newY, double newZ){
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }

    /**
     * @return the x field
     */
    public double getX(){
        return this.x;
    }
    /**
     * @return the y field
     */
    public double getY(){
        return this.y;
    }
    /**
     * @return the z field
     */
    public double getZ(){
        return this.z;
    }

    /**
     * @return the magnitude using the values in the x, y and z fields
     */
    public double magnitude(){
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    /**
     * @return the dot product using the x, y and z fields
     */
    public double dotProduct(Vector3d secondVect){
        return (this.x * secondVect.getX()) + (this.y * secondVect.getY()) + (this.z * secondVect.getZ());
    }

    /**
     * @param secondVect represents a Vector3d object used to 
     * add its fields to the local x, y and z fields
     * @return the sum of fields x, y and z with the fields of the inputted Vector3d object
     */
    public Vector3d add(Vector3d secondVect){
        double a = this.x + secondVect.getX();
        double b = this.y + secondVect.getY();
        double c = this.z + secondVect.getZ();
        Vector3d newVect = new Vector3d(a, b, c);
        return newVect;
    }
}
